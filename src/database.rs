use crate::Comment;
use derive_more::From;
use rusqlite::{params, Connection, Result};
use serde::Deserialize;
use std::fs;
use std::path::PathBuf;

pub struct Database {
    conn: Connection,
    pub settings: DatabaseSettings,
}

#[derive(Default, Clone, Deserialize)]
#[serde(rename_all = "camelCase")]
#[serde(default)]
pub struct DatabaseSettings {
    pub name_required: bool,
    pub email_required: bool,
    pub file: Option<String>,
    pub webhook: Option<String>,
}

#[derive(From, Debug)]
pub enum DatabaseCreationError {
    RusqliteError(rusqlite::Error),
    IoError(std::io::Error),
}

impl Database {
    pub fn new(
        testing: bool,
        name: &str,
        settings: DatabaseSettings,
    ) -> Result<Self, DatabaseCreationError> {
        let conn = if testing {
            Connection::open_in_memory()
        } else {
            let path = PathBuf::from(match &settings.file {
                Some(path) => path.clone(),
                None => format!("{name}.db"),
            });
            fs::create_dir_all(path.parent().unwrap())?;
            Connection::open(path)
        }?;
        conn.execute(
            "CREATE TABLE IF NOT EXISTS comment (
                id         INTEGER PRIMARY KEY,
                email      TEXT,
                author     TEXT,
                text       TEXT NOT NULL,
                timestamp  DATETIME DEFAULT CURRENT_TIMESTAMP,
                content_id TEXT NOT NULL,
                parent     INTEGER
            )",
            params![],
        )?;
        Ok(Self { conn, settings })
    }

    pub fn get_comments(&self, content_id: &str) -> Result<Vec<Comment>> {
        self.conn
            .prepare("SELECT id, author, email, text, timestamp FROM comment WHERE content_id=?1 AND parent IS NULL ORDER BY timestamp DESC")?
            .query_map(params![content_id], |row| {
                let id = row.get::<usize, Option<i64>>(0)?.unwrap();
                let replies = self.conn
                    .prepare("SELECT id, author, email, text, timestamp FROM comment WHERE parent=?1")?
                    .query_map(params![id], |row| {
                        Ok(Comment {
                            id: row.get(0)?,
                            author: row.get(1)?,
                            email: row.get(2)?,
                            text: row.get(3)?,
                            timestamp: row.get(4)?,
                            content_id: content_id.to_owned(),
                            parent: Some(id),
                            replies: Vec::new(), // no recursion
                        })
                    })?
                    .collect::<Result<Vec<Comment>>>()?;
                Ok(Comment {
                    id: Some(id),
                    author: row.get(1)?,
                    email: row.get(2)?,
                    text: row.get(3)?,
                    timestamp: row.get(4)?,
                    content_id: content_id.to_owned(),
                    parent: None,
                    replies,
                })
            })?
            .collect()
    }

    pub fn create_comment(&self, comment: &Comment) -> Result<()> {
        self.conn.execute(
            "INSERT INTO comment (author, email, text, content_id, parent) VALUES (?1, ?2, ?3, ?4, ?5)",
            params![
                &comment.author,
                &comment.email,
                &comment.text,
                &comment.content_id,
                &comment.parent,
            ],
        )?;
        Ok(())
    }
}
